# Use latest CentOS version
FROM centos:7
#Place the program in root folder
COPY ./ivan_json.bin/ /root/
# Add execute permissions
RUN chmod +x /root/ivan_json.bin
WORKDIR /root/
# Start the program
CMD [/root/ivan_json.bin]

